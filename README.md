# A resume

This project aims is to produce a three web page resume.
Two of them are straight forwards html pages (well if we consider straightforwards to make page render on a large variety of device and keep some consistence in space occupation)

The page is deployed via gitlab CI pages on [https://alexandremasselot.gitlab.io/resume](https://alexandremasselot.gitlab.io/resume)

The most interesting part, from the programming point of view, if the buzzword bingo: a feature timeline.
Some test (both unit and visual can be found under the test/ directory)

This project is also an experiment:

  * on the programming, trying to develop a medium complex web app without any heavy framework (angular & co)
  * unit and visual tests can be found [here](https://alexandremasselot.gitlab.io/resume/test/)
  * there is also a social component to the experiment. I'll tell you one day...

  