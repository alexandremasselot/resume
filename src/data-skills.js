

const skillsMeta = {
    'management+lead':
        {
            rank: 0,
            display: 'Management\n& Leadership',
            color: '#66c2a5'
        },
    'methodo':
        {
            rank: 1,
            display: 'Methodology',
            color: '#fc8d62'
        },
    'teaching':
        {
            rank: 2,
            display: 'Teaching',
            color: '#8da0cb'
        },
    'domain':
        {
            rank: 3,
            display: 'Domains',
            color: '#e78ac3'
        },
    'data viz':
        {
            rank: 4,
            display: 'Data\nvisualization',
            color: '#a6d854'
        },
    'techno':
        {
            rank: 5,
            display: 'Technique',
            color: '#FFC107'
        }
};