
function SkillTimelinetBuilder() {
}

SkillTimelinetBuilder.prototype.build = function (skills, metadata) {
    return _.chain(skills)
        .map((skill) => this.mashSkill(skill, metadata))
        .groupBy('category')
        .value();
};


SkillTimelinetBuilder.prototype.timeRange = function (skills) {
    return {
        start: _.chain(skills)
            .map('date_start')
            .min()
            .value(),
        end: _.chain(skills)
            .map('date_end')
            .max()
            .value(),
    }
};


SkillTimelinetBuilder.prototype.mashSkill = function (skill, metadata) {
    const tSkill = Object.create(skill);
    tSkill.date_start = moment(skill.date_start, 'DD/MM/YYYY');
    tSkill.date_end = moment(skill.date_end, 'DD/MM/YYYY');
    tSkill.date_reserve = skill.date_reserve && moment(skill.date_reserve, 'DD/MM/YYYY');
    tSkill.date_mode = skill.date_mode ? (moment(skill.date_mode, 'DD/MM/YYYY')) : undefined;
    tSkill.category_display = metadata[skill.category].display;
    if (skill.links) {
        tSkill.links = _.map(skill.links.split('\n'), function (l) {
            const t = l.split(';');
            return {
                url: t[0],
                title: t[1]
            };
        });
    } else {
        tSkill.links = undefined;
    }

    tSkill.metadata =
        metadata[tSkill.category];
    return tSkill;
};
