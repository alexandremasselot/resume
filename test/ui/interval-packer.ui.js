const packer = new IntervalPacker((o) => o[0], (o) => o[1]);

const konvas = {};
const canvasWidth = 400;
const canvasHeight = 100;

const plotIntervalPackingStack = (idTag, packing, iStep = undefined) => {
    const id = 'plot-stack-' + idTag;
    if (konvas[idTag] === undefined) {
        konvas[idTag] = {
            stage: new Konva.Stage({
                container: id,
                width: canvasWidth,
                height: canvasHeight
            }),
            layers: []
        };
        konvas[idTag].stage.on('mousemove', function (e) {
            const iLayer = Math.floor(konvas[idTag].layers.length * e.evt.layerX / canvasWidth);
            _.each(konvas[idTag].layers, (l, i) => {
                if (i <= iLayer) {
                    l.show();
                } else {
                    l.hide();
                }
            });

        });
        const div = document.getElementById(id);
        div.addEventListener('mouseout', function (e) {
            if (e.srcElement.id === id) {
                _.each(konvas[idTag].layers, (l) => l.show());
            }
        })
    }
    const konva = konvas[idTag];
    const layer = new Konva.Layer();
    konva.stage.add(layer);
    konva.layers.push(layer);


    const background = new Konva.Rect({
        x: 0,
        y: 0,
        width: canvasWidth,
        height: canvasHeight,
        name: 'background',
        fill: 'white',
        stroke: 'grey',
        strokeWidth: 1
    });
    layer.add(background);

    const scaleX = (x) => canvasWidth * (x - packing.min()) / (packing.max() - packing.min() + 1);
    const scaleY = (y) => canvasHeight * y / packing.nbLanes();
    _.chain(packing.list())
        .map(IntervalPacking.tagName)
        .each((interv) => {
            const x0 = scaleX(interv.start);
            const x1 = scaleX(interv.end + 1);
            const y0 = scaleY(interv.lane);
            const h = scaleY(1);
            const rect = new Konva.Rect({
                x: x0 + 1,
                y: y0 + 1,
                width: x1 - x0 - 2,
                height: h - 2,
                fill: '#00D2FF',
                strokeWidth: 0
            });
            layer.add(rect);
        })
        .value();

    if (iStep !== undefined) {
        const stepText = new Konva.Text({
            x: 10,
            y: canvasHeight - 20,
            text: `#${iStep}`,
            fontFamily: 'Calibri',
            fill: 'red'
        });
        layer.add(stepText);
    }

    layer.draw();
};

const showTiming = (idTag, timeMS) => {
    document.getElementById('timing-' + idTag).innerHTML = ` <span>Elapsed time: <strong>${timeMS}ms</strong></span>`;
};

const els12 = [[12, 13], [8, 9], [10, 12], [8, 12], [12, 14], [2, 4], [3, 3], [15, 17], [4, 8], [15, 16], [12, 15], [8, 10], [10, 14], [3, 6], [12, 17]];

setTimeout(() => {
    let t0 = new Date();
    const packingTopFirst = packer.pack(
        els12,
        method = 'topFirst'
    );
    plotIntervalPackingStack('packing-12-top-first', packingTopFirst);
    showTiming('packing-12-top-first', new Date().valueOf() - t0.valueOf());
}, 30);

setTimeout(() => {
    let t0 = new Date();
    const packingRandomLane = packer.pack(
        els12,
        method = 'randomLane'
    );
    plotIntervalPackingStack('packing-12-random-lane', packingRandomLane);
    showTiming('packing-12-random-lane', new Date().valueOf() - t0.valueOf());
}, 30);


setTimeout(() => {
    let t0 = new Date();
    let iStep = 0;
    const packingBalance = packer.pack(
        els12,
        method = 'balanceSpacing',
        {
            callbackLocalOptimal: (packing, score) => {
                plotIntervalPackingStack('packing-12-balance-spacing', packing, iStep++);
            }
        });
    plotIntervalPackingStack('packing-12-balance-spacing', packingBalance);
    showTiming('packing-12-balance-spacing', new Date().valueOf() - t0.valueOf());
}, 30);


